#!/usr/bin/env python3

from collections import defaultdict

import os
import select
import time

def gpio_exists(num:int):
    return os.path.exists(f'/sys/class/gpio/gpio{num}')

def gpio_export(num:int):
    if not gpio_exists(num):
        with open('/sys/class/gpio/export', 'wt') as f:
            f.write(str(num))

def gpio_unexport(num:int):
    if gpio_exists(num):
        with open('/sys/class/gpio/unexport', 'wt') as f:
            f.write(str(num))

def gpio_set_direction(num:int, is_out:bool):
    with open(f'/sys/class/gpio/gpio{num}/direction', 'wt') as f:
        f.write('out' if is_out else 'in')

def gpio_set_edge(num: int, mode:str):
    # mode: rising, falling, both
    with open(f'/sys/class/gpio/gpio{num}/edge', 'wt') as f:
        f.write(mode)

def gpio_get_value(num:int):
    with open(f'/sys/class/gpio/gpio{num}/value', 'rt') as f:
        return f.read()[0] == '1'

def gpio_set_value(num:int, value:bool):
    with open(f'/sys/class/gpio/gpio{num}/value', 'wt') as f:
        f.write('1' if value else '0')

def gpio_get_value_interrupt(num: int, cb: callable):
    fd = os.open(f'/sys/class/gpio/gpio{num}/value', os.O_RDONLY | os.O_NONBLOCK)

    # strip initial values
    _ = os.read(fd, 100)

    try:
        epoll = select.epoll()
        epoll.register(fd, select.EPOLLPRI | select.EPOLLET)

        status = defaultdict(bool)

        while 1:
            events = epoll.poll(-1)
            for fileno, event in events:
                if fileno == fd:
                    value = os.pread(fd, 10, 0)[0] ==  ord('1')
                    if status[fd] is not value:
                        status[fd] = value
                        cb(num, value)
    except KeyboardInterrupt:
        ...
    finally:
        epoll.unregister(fd)
        os.close(fd)


def main():
    print("""\
                      +--------+
                  3P3-|-1    2-|-5P0
   I2C_SDA(15)/GPIO82-|-3    4-|-5P0
   I2C_SCL(15)/GPIO81-|-5    6-|-GND
               GPIO83-|-7    8-|-GPIO87/UT_TXD(22)
                  GND-|-9   10-|-GPIO88/UT_RXD(22)
               GPIO84-|-11  12-|-GPIO89/PDM_OUT(64)
               GPIO85-|-13  14-|-GND
               GPIO86-|-15  16-|-GPIO90
                  3P3-|-17  18-|-GPIO65
 SPIO_MOSI(15)/GPIO63-|-19  20-|-GND
 SPIO_MISO(15)/GPIO64-|-21  22-|-GPIO66
 SPIO_SCLK(15)/GPIO61-|-23  24-|-GPIO62/SPIO_CS0(15)
                  GND-|-25  26-|-GPIO67/SPIO_CS1(16)
            RESERVED0-|-27  28-|-RESERVED1
              GPIO112-|-29  30-|-GND
              GPIO113-|-31  32-|-GPIO115/PDM_OUT(69)
  PDM_OUT(68)/GPIO114-|-33  34-|-GND
SPI1_MISO(22)/GPIO121-|-35  36-|-GPIO119/SPI1_CS0(22)
              GPIO117-|-37  38-|-GPIO120/SPI1_MOSI(22)
                  GND-|-39  40-|-GPIO118/SPI1_SCLK(22)
                      +--------+""")

    gpio = 117

    def callback(num:int, value: bool):
        print(f'GPIO #{num}: {value}')

    gpio_export(gpio)

    try:
        # Output
        gpio_set_direction(gpio, 1)
        gpio_set_value(gpio, 1)

        # Input
        gpio_set_direction(gpio, 0)
        value = gpio_get_value(gpio)

        gpio_set_edge(gpio, 'both')

        gpio_get_value_interrupt(gpio, callback)
    except KeyboardInterrupt:
        ...
    finally:
        gpio_unexport(gpio)

if __name__ == "__main__":
    main()