#!/usr/bin/env python3

import os
import time

def pwm_exists(num:int):
    return os.path.exists(f'/sys/class/pwm/pwmchip0/pwm{num}')

def pwm_export(num:int):
    if not pwm_exists(num):
        with open('/sys/class/pwm/pwmchip0/export', 'wt') as f:
            f.write(str(num))

def pwm_unexport(num:int):
    if pwm_exists(num):
        with open('/sys/class/pwm/pwmchip0/unexport', 'wt') as f:
            f.write(str(num))

def pwm_enable(num:int, enable:bool):
    with open(f'/sys/class/pwm/pwmchip0/pwm{num}/enable', 'wt') as f:
        f.write('1' if enable else '0')

def pwm_set_duty_cycle(num:int, duty_cycle_ms:int):
    with open(f'/sys/class/pwm/pwmchip0/pwm{num}/duty_cycle', 'wt') as f:
        f.write(str(duty_cycle_ms * 1000 * 1000))

def pwm_set_period(num: int, period_ms:int):
    with open(f'/sys/class/pwm/pwmchip0/pwm{num}/period', 'wt') as f:
        f.write(str(period_ms * 1000 * 1000))

def main():
    print("""\
                      +--------+
                  3P3-|-1    2-|-5P0
   I2C_SDA(15)/GPIO82-|-3    4-|-5P0
   I2C_SCL(15)/GPIO81-|-5    6-|-GND
               GPIO83-|-7    8-|-GPIO87/UT_TXD(22)
                  GND-|-9   10-|-GPIO88/UT_RXD(22)
               GPIO84-|-11  12-|-GPIO89/PDM_OUT(64)
               GPIO85-|-13  14-|-GND
               GPIO86-|-15  16-|-GPIO90
                  3P3-|-17  18-|-GPIO65
 SPIO_MOSI(15)/GPIO63-|-19  20-|-GND
 SPIO_MISO(15)/GPIO64-|-21  22-|-GPIO66
 SPIO_SCLK(15)/GPIO61-|-23  24-|-GPIO62/SPIO_CS0(15)
                  GND-|-25  26-|-GPIO67/SPIO_CS1(16)
            RESERVED0-|-27  28-|-RESERVED1
              GPIO112-|-29  30-|-GND
              GPIO113-|-31  32-|-GPIO115/PDM_OUT(69)
  PDM_OUT(68)/GPIO114-|-33  34-|-GND
SPI1_MISO(22)/GPIO121-|-35  36-|-GPIO119/SPI1_CS0(22)
              GPIO117-|-37  38-|-GPIO120/SPI1_MOSI(22)
                  GND-|-39  40-|-GPIO118/SPI1_SCLK(22)
                      +--------+""")

    pwm = 2  # 0: PDM_OUT(64), 1: PDM_OUT(68), 2: PDM_OUT(69)

    pwm_export(pwm)

    try:
        pwm_enable(pwm, False)

        pwm_set_period(pwm, 200)
        pwm_set_duty_cycle(pwm, 100)

        pwm_enable(pwm, True)

        time.sleep(3)
    except KeyboardInterrupt:
        ...
    finally:

        pwm_enable(pwm, False)
        pwm_unexport(pwm)

if __name__ == "__main__":
    main()

