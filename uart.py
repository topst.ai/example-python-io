#!/usr/bin/env python3

from enum import IntEnum
import fcntl
import os
import termios
import time


UART_DEV = "/dev/ttyAMA2"


class TCATTRS(IntEnum):
    IFLAG = 0
    OFLAG = 1
    CFLAG = 2
    LFLAG = 3
    ISPEED = 4
    OSPEED = 5


def uart_set_speed(fd:int, speed:int):
    baudrates = {
        115200: termios.B115200,
        57600: termios.B57600,
        38400: termios.B38400,
        19200: termios.B19200,
        9600: termios.B9600,
        4800: termios.B4800,
        2400: termios.B2400,
        1200: termios.B1200,
    }

    # https://github.com/python/cpython/blob/main/Modules/termios.c#L166
    attrs = termios.tcgetattr(fd)

    attrs[TCATTRS.IFLAG] &= ~(termios.INPCK | termios.ISTRIP | termios.INLCR | termios.IGNCR | termios.ICRNL | termios.IXON)

    attrs[TCATTRS.ISPEED] = baudrates[speed]
    attrs[TCATTRS.OSPEED] = baudrates[speed]

    attrs[TCATTRS.CFLAG] &= ~termios.CSIZE
    attrs[TCATTRS.CFLAG] |= termios.CS8 | termios.CLOCAL | termios.CREAD

    attrs[TCATTRS.LFLAG] &= ~(termios.ICANON | termios.ECHO | termios.ECHOE | termios.ISIG)

    attrs[TCATTRS.OFLAG] &= ~termios.OPOST

    termios.tcsetattr(fd, termios.TCSANOW, attrs)

def uart_write_str(fd:int, data:str):
    os.write(fd, data.encode('utf-8'))

def uart_read_str(fd:int):
    return os.read(fd, 1024).decode('utf-8')

def main():
    fd = os.open(UART_DEV, os.O_RDWR | os.O_NOCTTY)
    # fd = os.open(UART_DEV, os.O_RDWR | os.O_NOCTTY | os.O_NONBLOCK)
    uart_set_speed(fd, 115200)

    try:
        uart_write_str(fd, "Hello")
        time.sleep(1)
        data = uart_read_str(fd)
        print('>>', data)
    finally:
        os.close(fd)

if __name__ == "__main__":
    main()